import matplotlib.pyplot as plt
import numpy as np
import math
import yaml
from itertools import accumulate

X_LABEL = "Flow rate (m^3/h)"
Y_LABEL = "Maximum load (m^3/h)"


def add_inset(ax, data, inset_position, yticks, xticks, y_tick_label):
    axins = ax.inset_axes(inset_position, transform=ax.transData)

    for line in data:
        xdata, ydata, color = line
        axins.plot(xdata, ydata, color=color, marker="o")

    axins.set_xlim(196.5, 197.5)
    axins.set_ylim(6.92e-2, 6.98e-2)
    axins.set_yticks(yticks[-2:])
    axins.set_yticklabels(y_tick_label[-2:])

    xticks.sort()
    axins.set_xticks(xticks[-2:])

    ax.indicate_inset_zoom(axins)

    for i in range(2):
        yticks.pop()
        y_tick_label.pop()
        xticks.pop()


def composite_curve_graph(filename, data, title, segment_annotations=[]):
    fig, ax = plt.subplots()
    fig.tight_layout(pad=5.5)
    fig.set_size_inches(10, 8, forward=True)

    ax.set_title(title, {"fontsize": 18, "fontweight": "bold"})
    ax.set_ylabel(Y_LABEL, {"fontsize": 14, "fontweight": "bold"})
    ax.set_xlabel(X_LABEL, {"fontsize": 14, "fontweight": "bold"})

    xticks = []
    yticks = []
    curves = []

    for line in data:
        xdata, ydata, color = line
        (c,) = ax.plot(xdata, ydata, color=color, marker="o")
        curves.append(c)

        xticks = xticks + xdata
        yticks = yticks + ydata

        # Add annotations
        for i in range(1, len(segment_annotations) + 1):
            text_y = (ydata[i] + ydata[i - 1]) / 3
            text_x = (xdata[i] + xdata[i - 1]) / 2

            plt.text(text_x, text_y, segment_annotations[i - 1], fontsize=12)

    if len(data) > 1:
        ax.legend(
            (curves[0], curves[1]), ("sink", "source"), loc="lower right", shadow=True
        )

    yticks.sort()
    if yticks[0] == yticks[1]:
        yticks.pop(0)

    y_tick_label = ["{:5.2e}".format(y) for y in yticks]
    y_tick_label[0] = 0

    if filename == "graph_6":
        add_inset(ax, data, [20, 4.5e-2, 80, 0.02], yticks, xticks, y_tick_label)

    ax.set_yticks(yticks)
    ax.set_yticklabels(y_tick_label)
    ax.set_xticks(xticks)

    # display the plot
    plt.grid(True)
    plt.savefig(filename + ".pdf", pad_inches=0.3)
    # plt.show()


yaml_file = open("./data.yml", "r")
data = yaml.load(yaml_file)

for g_data in data["data"]:
    for graph_name, data in g_data.items():
        print(data)
        points = []
        for curve in data["curves"]:
            print(curve)
            print(data["curves"][curve])
            c_data = data["curves"][curve]["points"]
            xdata = list(accumulate([x for [x, _] in c_data]))
            ydata = list(accumulate([y for [_, y] in c_data]))
            points.append([xdata, ydata, f"tab:{data['curves'][curve]['color']}"])
        composite_curve_graph(graph_name, points, data["title"], data["annotations"])
